# A hello world julia script
#=
A multi-line
comment
here!
Nathan North
=#

using DataFrames
using Plots
x = 1:10;
y = rand(10) .* 10; # These are the plotting data
plot(x,y)
# savefig("myplot.png")

#df = DataFrame(A = 1:4, B = [1, 5, 7, 8])

#for item in df.A
#    print(item)
#end


struct MyType
    a::Int64
    b::Float64
end

x = MyType(3, 4)
x.a
