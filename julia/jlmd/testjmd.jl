using Pkg
Pkg.add.(["Plots", "DSP", "Weave"])

in = "markdown/test.jmd"
out = :pwd
style = "pdf"

using Weave

if style == "pdf"
      weave("markdown/test.jmd", out_path=:pwd, doctype = "md2pdf")
elseif style == "html"
      weave("markdown/test.jmd", out_path=:pwd, doctype = "md2html")
elseif style == "md"
      weave("markdown/test.jmd", out_path=:pwd, doctype = "pandoc")
end
